var thymeleaf = require('/lib/xp/thymeleaf'); // Import the thymeleaf library
var contentLib = require('/lib/xp/content');
var portalLib = require('/lib/xp/portal');
var jsonPath = require('/lib/openxp/jsonpath');
var nbaLib = require('/lib/openxp/nba/index');
var httpClient = require('/lib/http-client');
var renderHelper = require('/lib/renderHelper/index');
var helper = require('/lib/openxp/helper');
var util = require('/lib/enonic/util');

var view = resolve('personalized.html');

var getFFAMatch = function(ffa, nbaResponseBody){
    var ffaMatch = [];
    if (ffa !== undefined && nbaResponseBody){
        ffaMatch = util.data.trimArray(jsonPath.process(nbaResponseBody,
            "$..basis[?(@.ffa=="+ffa+")]"));
    }
    return ffaMatch.length > 0;
};

var getGenderMatch = function (params) {
    var genderMatch = [];
    if (!params.gender || !(params.nbaResponseBody || params.req.cookies["personalizedGender"])) return genderMatch;

    var gender = params.gender;

    //Check if gender is overridden in cookie, if not check the nba response for match
    if (params.req && isPreviewOrEditMode(params) && params.req.cookies["personalizedGender"] !== undefined){
        if (params.req.cookies["personalizedGender"] === gender){
            genderMatch.push(gender);
        }

    }else if (!isPreviewOrEditMode({req: params.req})){
        var nbaResponseBody = params.nbaResponseBody;
        if (gender && nbaResponseBody){
            genderMatch = jsonPath.process(nbaResponseBody,
                "$..basis[?(@.gender=='" + gender + "')]");
        }
    }

    return genderMatch;
};

var getAgeMatch = function (params) {
    var ageMatch = [];

    if (!(params.bornEarliest || params.bornLatest) || !(params.nbaResponseBody || params.req.cookies["personalizedBirthyear"])) return ageMatch;

    var bornEarliest = params.bornEarliest;
    var bornLatest = params.bornLatest;
    var nbaResponseBody = params.nbaResponseBody;
    if (params.req && isPreviewOrEditMode(params) && params.req.cookies["personalizedBirthyear"] !== undefined){
        var birthyear = params.req.cookies["personalizedBirthyear"];

        if (util.value.isInt(bornEarliest) && util.value.isInt(birthyear) && (Number(birthyear) >= Number(bornEarliest))){
            ageMatch.push(birthyear);
        }else if (util.value.isInt(bornLatest) && util.value.isInt(birthyear) && (Number(birthyear <= Number(bornLatest)))){
            ageMatch.push(birthyear);
        }
    }else if (!isPreviewOrEditMode({req: params.req})){
        if (bornEarliest && bornLatest) {//born between
            ageMatch = jsonPath.process(nbaResponseBody,
                "$..basis[?(" +
                "@.birthyear >= '" + bornEarliest + "' && " +
                "@.birthyear <= '" + bornLatest + "')]");
        }
        else if (bornEarliest) {
            ageMatch = jsonPath.process(nbaResponseBody,
                "$..basis[?(" +
                "@.birthyear >='" + bornEarliest + "')]");
        }
        else if (bornLatest) {
            ageMatch = jsonPath.process(nbaResponseBody,
                "$..basis[?(" +
                "@.birthyear <='" + bornLatest + "')]");
        }
    }

    return ageMatch;
};


var getNbaMatch = function (params) {
    var keyMatches = [];
    var nbaKeys = params.nbaKeys;
    var nbaResponseBody = params.nbaResponseBody;

    if (!(!helper.isEmptyArray(nbaKeys) || params.req.cookies["personalizedNba"])) return keyMatches;

    if (params.req && isPreviewOrEditMode(params) && params.req.cookies["personalizedNba"] !== undefined){
        if (nbaKeys.indexOf(params.req.cookies["personalizedNba"]) !== -1){
            keyMatches.push({
                type:params.req.cookies["personalizedNba"],
                score:0
            });
        }
    }else if (!isPreviewOrEditMode({req: params.req})){
        nbaKeys.forEach(function (key) {
            var keyMatch = jsonPath.process(nbaResponseBody,
                "$..recommendations[?(" +
                "@.status=='ACTIVE' && " +
                "@.active==true && " +
                "@.validConsent==true &&" +
                "@.type == '" + key + "')]");
            if (keyMatch && keyMatch.length > 0) {
                keyMatch.forEach(function (k) {
                    keyMatches.push(k);
                });
            }
        });
    }
    return keyMatches;
};

exports.get = function (req) {
    var model = {};

    var component = portalLib.getComponent();
    var defaultContentKey = component.config.content;
    if (!defaultContentKey) {
        return returnWithModel(model);
    }

    var personalizations = nbaLib.getPersonalizations({req:req});

    //No configured personalizations
    if (!personalizations) {
        log.debug('No configured personalizations');
        return renderDefaultContentBasedOnType({
            key: defaultContentKey,
            personalizations: personalizations,
            req:req});
    }

    //No matching personalizations (nba / sbProduct / parameters or cookies)
    if (!personalizations.campaignMatch && !personalizations.svProductMatch && !personalizations.checkForNBAMatch) {
        log.debug('No matching personalizations');
        return renderDefaultContentBasedOnType({
            key: defaultContentKey,
            personalizations: personalizations,
            req:req});
    }

    /*
    var getCookieValueByNameOrUseFallback = function (params) {
        if (!params.cookieName || !req.cookies[params.cookieName]) return params.fallback;
        return req.cookies[params.cookieName] || params.fallback;
    };
    */

    //Will be true if stb_id is set and there are configured nbas in personalized content
    if (personalizations.checkForNBAMatch) {
        var response = httpClient.request({
            url: 'http://localhost:8080/app/no.stb.web/nba/' + personalizations.stbId,
            method: 'GET',
            connectionTimeout: 5000,
            readTimeout: 5000,
            body: '{"stb_id": ' + personalizations.stbId + '}',
            contentType: 'application/json'
        });

        var nbaResponseBody = JSON.parse(response.body);

        //log.debug('nba response %s', nbaResponseBody);

        var bestMatch = {};

        //TODO: Test code and implement ffa
        for (var i = 0; i < personalizations.nbas.length; i++) {

            //For this to be a match each given property must match (age, ffa, gender, nba-code)
            var nbaKeys = util.data.trimArray(personalizations.nbas[i].key);
            var gender = personalizations.nbas[i].gender;
            var ffa = personalizations.nbas[i].ffa;
            var bornEarliest = personalizations.nbas[i].bornEarliest;
            var bornLatest = personalizations.nbas[i].bornLatest;
            var contentRef = personalizations.nbas[i].contentRef;
            if (!contentRef) continue;
            //TODO: What if age is not set, should one continue here?
            var ageMatch = getAgeMatch({req: req, bornEarliest: bornEarliest, bornLatest: bornLatest, nbaResponseBody: nbaResponseBody});
            var genderMatch = getGenderMatch({req:req, gender: gender, nbaResponseBody:nbaResponseBody});

            var isAgeMatch = util.data.trimArray(ageMatch).length > 0;
            var isGenderMatch = util.data.trimArray(genderMatch).length > 0;
            var isFFAMatch = getFFAMatch(ffa, nbaResponseBody);

            if ((bornLatest || bornEarliest) && !isAgeMatch){
                continue;
            }

            if (gender && !isGenderMatch){
                continue;
            }

            if (ffa!==undefined && !isFFAMatch){
                continue;
            }
            var keyMatches = getNbaMatch({req: req, nbaKeys:nbaKeys, nbaResponseBody:nbaResponseBody});
            var isNbaMatch = util.data.trimArray(keyMatches).length > 0;
            if (nbaKeys.length > 0 && !isNbaMatch) continue;

            // variables to hold the values for data-personalized-[source|key] attributes
            //TODO: Document how data attributes are created and parsed in analytics somewhere f.x. in wiki
            var matchSource = [];
            var matchKey = [] ;
            //Iterate the nba matches to find the best one
            if (keyMatches.length > 0) {//keyMatches are scored, find the best one
                for (var m = 0; m < keyMatches.length; m++) {
                    if (helper.isEmptyObject(bestMatch)) {
                        bestMatch = keyMatches[m];
                        bestMatch.contentRef = contentRef;
                        matchSource.push('nba');
                        matchKey.push(keyMatches[m].type);
                    } else {
                        if (bestMatch.score < keyMatches[m].score) {
                            bestMatch = keyMatches[m];
                            bestMatch.contentRef = contentRef;
                            matchSource.push('nba');
                            matchKey.push(keyMatches[m].type);
                        }
                    }
                }
            }
            //TODO: Refactor and test this code so it is understandable (made in a hurry
            if (isAgeMatch && bornEarliest && bornLatest){
                matchSource.push('age');
                matchKey.push('bornBetween'+bornEarliest + '-' + bornLatest);
                bestMatch.score = 0;
                if (helper.isEmptyArray(nbaKeys)){
                    bestMatch.contentRef = contentRef;
                }
            }else if (isAgeMatch && bornEarliest){
                matchSource.push('age');
                matchKey.push('bornEarliest'+bornEarliest);
            }else if (isAgeMatch && bornLatest){
                matchSource.push('age');
                matchKey.push('bornLatest'+bornLatest);
            }
            if (isGenderMatch && gender){
                bestMatch.gender = gender;
                matchSource.push('gender');
                matchKey.push(gender);
            }
            if (ffa !== undefined && isFFAMatch){
                bestMatch.ffa = ffa;
                matchSource.push('ffa');
                matchKey.push(ffa);
            }

            if ((ageMatch || genderMatch) && helper.isEmptyArray(nbaKeys) && !bestMatch.contentRef){
                bestMatch.contentRef = contentRef;
                bestMatch.score = 0;
            }

            if (matchSource && matchSource.length > 0){
                bestMatch.attrAppend = ',data-personalized-source='+matchSource.join('--');
            }
            if (matchKey && matchKey.length > 0){
                bestMatch.attrAppend += ',data-personalized-key='+matchKey.join('--');
            }
        }


        if (bestMatch.contentRef) {
            return renderPersonalizedContentBasedOnType({
                key: bestMatch.contentRef,
                matches: [bestMatch],
                personalizations: personalizations,
                req:req});
        }
    }
    if (personalizations.svProductMatch && personalizations.personalizedContentRef) {
        return renderPersonalizedContentBasedOnType({
            key: personalizations.personalizedContentRef,
            matches: personalizations.matches,
            personalizations: personalizations,
            req:req
        });
    }

    if (personalizations.campaignMatch && personalizations.personalizedContentRef) {
        return renderPersonalizedContentBasedOnType({
            key: personalizations.personalizedContentRef,
            matches: personalizations.matches,
            personalizations: personalizations,
            req:req
        });
    }
    return renderDefaultContentBasedOnType({
        key: defaultContentKey,
        req:req});

};

function returnWithModel(model) {
    return {
        body: thymeleaf.render(view, model)
    }
}

var renderDefaultContentBasedOnType = function (params) {
    if (!params.key) return;
    var defaultContent = contentLib.get({key: params.key});
    if (defaultContent.type === 'portal:fragment') {
        return handleFragment({
            isPersonalized: false,
            content: defaultContent,
            data: defaultContent.page.fragment.config,
            personalizations: params.personalizations,
            req: params.req
        });
    }
};

//TODO: Do some testing here for case when previewing a fragment in Content Studio
var renderPersonalizedContentBasedOnType = function (params) {
    if (!params.key) return;
    var personalizedContent = contentLib.get({key: params.key});
    if (!personalizedContent) return;
    if (personalizedContent.type === 'portal:fragment') {
        return handleFragment({
            isPersonalized: true,
            content: personalizedContent,
            matches: params.matches,
            data: personalizedContent.page.fragment.config,
            personalizations: params.personalizations,
            req: params.req
        });
    }
};

var handleFragment = function (params) {

    if (!params.content) return;
    var content = params.content;
    var descriptor = content.page.fragment.descriptor;
    var contentTypeName = descriptor.split(':')[1];

    var guessedContenttypeExports;
    try {
        guessedContenttypeExports = require('/site/parts/' + contentTypeName + '/exports');
    }catch (e){
        log.warning('Error when guessing contenttype in personalization %s', e);
    }
    if (!guessedContenttypeExports) return;

    var model = guessedContenttypeExports.getModel({data: params.data}) || guessedContenttypeExports.getDefaultModel();


    if (params.isPersonalized){
        model.attrAppend += ',data-ispersonalized=true';
        model.attrAppend += ',data-personalized-fragment='+content._name;
    }else{
        model.attrAppend += ',data-ispersonalized=false';
        model.attrAppend += ',data-default-fragment='+content._name;
    }

    var component = portalLib.getComponent();


    if (component.path){//a component in a page has a path
        model.attrAppend += ',data-componentid='+helper.removeBadChar(component.path);
    }else if (component.descriptor === app.name + ':personalized'){//when editing a fragment, it has no path
        model.attrAppend += ',data-componentid=singlePersonalizedFragment';
    }

    var matches = params.matches;
    if (matches) {
        for (var m = 0; m < matches.length; m++) {
            if (matches[m].attrAppend){
                model.attrAppend += matches[m].attrAppend;
            }
        }
    }

    var view = guessedContenttypeExports.view;
    var whoami = app.name + ':' + contentTypeName;
    var html = renderHelper.renderWithModel({whoami: whoami, view: view, model: model});
    return html
};

var isPreviewOrEditMode = function (params) {
    return params.req && params.req.mode && (params.req.mode === 'preview' || params.req === 'edit');
};