let replacePersonalizedComponent = function (pc) {
    var componentToReplace = document.querySelector("[data-componentid=" + pc.id + "]");
    if (!componentToReplace) {
        return;
    }
    var parentNode = componentToReplace.parentNode;
    if (!parentNode) {
        return;
    }
    $.ajax({
        url: pc.url, success: function (result) {
            if (pc.url.indexOf('/_/') !== -1) {//url was to a component
                $(result).insertBefore(componentToReplace);
                parentNode.removeChild(componentToReplace);
            } else {//url was to page, when editing / previewing a personalized fragment
                var componentToInsert = $(result).filter("[data-componentid=" + pc.id + "]");
                componentToInsert.insertBefore(componentToReplace);
                parentNode.removeChild(componentToReplace);
            }
        }
    });
};
document.addEventListener("DOMContentLoaded", function (event) {
    var personalizedGenderSelect = document.querySelector("#personalizedGenderSelect");
    var personalizedBirthyearInput = document.querySelector("#personalizedBirthyearInput");
    var personalizedNBASelect = document.querySelector("#personalizedNBASelect");

    if (personalizedGenderSelect) {
        var cookieGenderOrNull = (document.cookie.match(/^(?:.*;)?\s*personalizedGender\s*=\s*([^;]+)(?:.*)?$/) || [, null])[1];
        if (cookieGenderOrNull !== null) {
            personalizedGenderSelect.value = cookieGenderOrNull
        }
        personalizedGenderSelect.addEventListener('change', function (e) {
            var gender = e.target.value;
            document.cookie = "personalizedGender=" + gender + ";path=/";
            if (typeof personalizedComponents !== 'undefined') {
                personalizedComponents.forEach(function (pc) {
                    if (pc.personalizedForGender === true) {
                        replacePersonalizedComponent(pc);
                    }

                });
            }
        })
    }

    if (personalizedBirthyearInput) {
        var cookieBirthyearOrNull = (document.cookie.match(/^(?:.*;)?\s*personalizedBirthyear\s*=\s*([^;]+)(?:.*)?$/) || [, null])[1];
        if (cookieBirthyearOrNull !== null) {
            personalizedBirthyearInput.value = cookieBirthyearOrNull
        }

        personalizedBirthyearInput.addEventListener('change', function (e) {
            var birthyear = e.target.value;
            document.cookie = "personalizedBirthyear=" + birthyear + ";path=/";
            if (typeof personalizedComponents !== 'undefined') {
                personalizedComponents.forEach(function (pc) {
                    if (pc.personalizedForBirthyear === true) {
                        replacePersonalizedComponent(pc);
                    }
                });
            }
        });
    }

    if (personalizedNBASelect) {
        var cookieNbaOrNull = (document.cookie.match(/^(?:.*;)?\s*personalizedNba\s*=\s*([^;]+)(?:.*)?$/) || [, null])[1];
        if (cookieNbaOrNull !== null) {
            personalizedNBASelect.value = cookieNbaOrNull;
        }
        personalizedNBASelect.addEventListener('change', function (e) {
            var nba = e.target.value;
            document.cookie = "personalizedNba=" + nba + ";path=/";
            if (typeof personalizedComponents !== 'undefined') {
                personalizedComponents.forEach(function (pc) {
                    if (pc.personalizedForNba === true) {
                        replacePersonalizedComponent(pc);
                    }
                });
            }
        });
    }
});
