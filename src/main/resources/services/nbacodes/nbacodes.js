var portalLib = require('/lib/xp/portal');

exports.get = handleGet;

function handleGet(req) {

    var params = parseparams(req.params);
    var body = createresults(getItems(), params);
    return {
        contentType: 'application/json',
        body: body
    }
}

function parseparams(params) {

    var query = params['query'],
        ids, start, count;

    try {
        ids = JSON.parse(params['ids']) || []
    } catch (e) {
        log.warning('Invalid parameter ids: %s, using []', params['ids']);
        ids = [];
    }

    try {
        start = Math.max(parseInt(params['start']) || 0, 0);
    } catch (e) {
        log.warning('Invalid parameter start: %s, using 0', params['start']);
        start = 0;
    }

    try {
        count = Math.max(parseInt(params['count']) || 15, 0);
    } catch (e) {
        log.warning('Invalid parameter count: %s, using 15', params['count']);
        count = 15;
    }

    return {
        query: query,
        ids: ids,
        start: start,
        end: start + count,
        count: count
    }
}

function createresults(items, params, total) {

    var body = {};

    var hitCount = 0, include;
    body.hits = items.sort(function (hit1, hit2) {
        if (!hit1 || !hit2) {
            return !!hit1 ? 1 : -1;
        }
        return hit1.displayName.localeCompare(hit2.displayName);
    }).filter(function (hit) {
        include = true;

        if (!!params.ids && params.ids.length > 0) {
            include = params.ids.some(function (id) {
                return id == hit.id;
            });
        } else if (!!params.query && params.query.trim().length > 0) {
            var qRegex = new RegExp(params.query, 'i');
            include = qRegex.test(hit.displayName) || qRegex.test(hit.description) || qRegex.test(hit.id);
        }

        if (include) {
            hitCount++;
        }
        return include && hitCount > params.start && hitCount <= params.end;
    });
    body.count = Math.min(params.count, body.hits.length);
    body.total = params.query ? hitCount : (total || items.length);

    return body;
}



function getItems() {
    return [{
            id: "ask_delphi",
            displayName: "ASK Delphi",
            description: "ask_delphi"
        },
        {
            id: "ask_storebrand",
            displayName: "ASK Storebrand",
            description: "ask_storebrand"
        },
        {
            id: "ask_savings_delphi",
            displayName: "ASK spareavtale Delphi",
            description: "ask_savings_delphi"
        },
        {
            id: "ask_purchase_storebrand",
            displayName: "ASK nykjøp Storebrand",
            description: "ask_purchase_storebrand"
        },
        {
            id: "ask_purchase_delphi",
            displayName: "ASK nykjøp Delphi ",
            description: "ask_purchase_delphi"
        },
        {
            id: "ask_savings_storebrand",
            displayName: "ASK spareavtale Storebrand",
            description: "ask_savings_storebrand"
        },
        {
            id: "ips_pm",
            displayName: "IPS",
            description: "ips_pm"
        },
        {
            id: "ips_pm_presale_completion",
            displayName: "IPS presale completion",
            description: "ips_pm_presale_completion"
        },
        {
            id: "ips_bm",
            displayName: "IPS ansatt",
            description: "ips_bm"
        },
        {
            id: "aksjesparekonto",
            displayName: "ASK tilflytt",
            description: "aksjesparekonto"
        },
        {
            id: "fmi",
            displayName: "Fripolise med investeringsvalg",
            description: "fmi"
        },
        {
            id: "ffa",
            displayName: "Storebrand Fordel",
            description: "ffa"
        },
        {
            id: "volumesavings",
            displayName: "Investering",
            description: "volumesavings"
        },
        {
            id: "helseforsikring_pm",
            displayName: "Helseforsikring PM",
            description: "helseforsikring_pm"
        },
        {
            id: "carinsurance",
            displayName: "Bilforsikring",
            description: "carinsurance"
        },
        {
            id: "dpt",
            displayName: "Ditt pensjonstall",
            description: "dpt"
        },
        {
            id: "blu40",
            displayName: "Boliglån  BLU40",
            description: "blu40"
        },
        {
            id: "sustainablemortgageloan",
            displayName: "Bærekraftig boliglån",
            description: "sustainablemortgageloan"
        },
        {
            id: "pkbmove",
            displayName: "PKB tilflytt ",
            description: "pkbmove"
        },
        {
            id: "endre_spareprofil_itp",
            displayName: "Endre spareprofil ITP",
            description: "endre_spareprofil_itp"
        },
        {
            id: "endre_spareprofil_pkb",
            displayName: "Endre spareprofil PKB",
            description: "endre_spareprofil_pkb"
        },
        {
            id: "childinsurance",
            displayName: "Barneforsikring",
            description: "childinsurance"
        },
        {
            id: "dodsfallforsikring",
            displayName: "Dødsfallforsikring",
            description: "dodsfallforsikring"
        },
        {
            id: "disabilityinsurance",
            displayName: "Uføreforsikring",
            description: "disabilityinsurance"
        },
        {
            id: "pensjonskonto",
            displayName: "Plasseringskonto pensjon",
            description: "pensjonskonto"
        },
        {
            id: "stbpluss",
            displayName: "Storebrand pluss",
            description: "stbpluss"
        },
        {
            id: "ajourhold_pensjon",
            displayName: "Ajourhold",
            description: "ajourhold_pensjon"
        },
        {
            id: "ajourhold_pensjon_pf",
            displayName: "Ajourhold",
            description: "ajourhold_pensjon_pf"
        },
        {
            id: "ajourhold_pf",
            displayName: "Ajourhold",
            description: "ajourhold_pf"
        },
        {
            id: "ajourhold",
            displayName: "Ajourhold",
            description: "ajourhold"
        },
        {
            id: "yrkesskadeforsikring",
            displayName: "Personalforsikring YS",
            description: "yrkesskadeforsikring"
        },
        {
            id: "grouplifeinsurance",
            displayName: "Personalforsikring gruppeliv",
            description: "grouplifeinsurance"
        },
        {
            id: "helseforsikring",
            displayName: "Helseforsikring",
            description: "helseforsikring"
        },
        {
            id: "mortgageloan",
            displayName: "Boliglån",
            description: "mortgageloan"
        },
        {
            id: "disabilitypension",
            displayName: "Uførepensjon",
            description: "disabilitypension"
        },
        {
            id: "loaninsurance",
            displayName: "Gjeldsforsikring",
            description: "loaninsurance"
        },
        {
            id: "increased_savings",
            displayName: "Økt sparesats",
            description: "increased_savings"
        },
        {
            id: "nho1a",
            displayName: "NHO Gruppe 1A",
            description: "nho1a"
        },
        {
            id: "nho1b",
            displayName: "NHO Gruppe 1B",
            description: "nho1b"
        },
        {
            id: "nho2a",
            displayName: "NHO Gruppe 2A",
            description: "nho2a"
        },
        {
            id: "nho2b",
            displayName: "NHO Gruppe 2B",
            description: "nho2b"
        },
        {
            id: "nho3a",
            displayName: "NHO Gruppe 3A",
            description: "nho3a"
        },
        {
            id: "nho3b",
            displayName: "NHO Gruppe 3B",
            description: "nho3b"
        },
        {
            id: "nho4",
            displayName: "NHO Gruppe 4",
            description: "nho4"
        },
        {
            id: "kritisksykdom",
            displayName: "Kritisk sykdom",
            description: "kritisksykdom"
        },
        {
            id: "savings_for_children",
            displayName: "Sparing til barn/barnebarn",
            description: "savings_for_children"
        },
        {
            id: "endre_spareprofil",
            displayName: "Endre spareprofil",
            description: "endre_spareprofil"
        },
        {
            id: "house_contentsinsurance",
            displayName: "Innboforsikring",
            description: "house_contentsinsurance"
        },
        {
            id: "traktorforsikring",
            displayName: "Traktorforsikring",
            description: "traktorforsikring"
        },
        {
            id: "elbilforsikring",
            displayName: "Elbilforsikring",
            description: "elbilforsikring"
        },
        {
            id: "ips_250",
            displayName: "IPS 250",
            description: "ips_250"
        },
        {
            id: "sustainablesavings",
            displayName: "Hva er bærekraftig sparing",
            description: "sustainablesavings"
        },
        {
            id: "verdisakforsikring",
            displayName: "Verdisakforsikring",
            description: "verdisakforsikring"
        },
        {
            id: "batforsikring",
            displayName: "Båtforsikring",
            description: "batforsikring"
        },
        {
            id: "tilhengerforsikring",
            displayName: "Tilhengerforsikring",
            description: "tilhengerforsikring"
        },
        {
            id: "animalinsurance",
            displayName: "Hund- og kattforsikring",
            description: "animalinsurance"
        },
        {
            id: "mcatvsnoscooterforsikring",
            displayName: "MC-, ATV- og snøscooterforsikring",
            description: "mcatvsnoscooterforsikring"
        },
        {
            id: "cabininsurance",
            displayName: "Hytteforsikring",
            description: "cabininsurance"
        },
        {
            id: "fastrenteinnskudd",
            displayName: "Fastrenteinnskudd",
            description: "fastrenteinnskudd"
        },
        {
            id: "ulykkesforsikring",
            displayName: "Ulykkesforsikring",
            description: "ulykkesforsikring"
        },
        {
            id: "travelinsurance",
            displayName: "Reiseforsikring",
            description: "travelinsurance"
        },
        {
            id: "veterankjoretoyforsikring",
            displayName: "Veterankjøretøyforsikring",
            description: "veterankjoretoyforsikring"
        },
        {
            id: "mopedforsikring",
            displayName: "Mopedforsikring",
            description: "mopedforsikring"
        },
        {
            id: "campingvognforsikring",
            displayName: "Campingvognforsikring",
            description: "campingvognforsikring"
        },
        {
            id: "fastrentelan",
            displayName: "Fastrentelån",
            description: "fastrentelan"
        },
        {
            id: "personrisiko",
            displayName: "Gjennomgang personrisiko",
            description: "personrisiko"
        },
        {
            id: "houseinsurance",
            displayName: "Husforsikring",
            description: "houseinsurance"
        },
        {
            id: "cabin_contentsinsurance",
            displayName: "Hytteinnboforsikring",
            description: "cabin_contentsinsurance"
        },
        {
            id: "bfa",
            displayName: "BFA",
            description: "bfa"
        },
        {
            id: "pea",
            displayName: "Pensjonsgjennomgang ansatt ",
            description: "pea"
        },
        {
            id: "storkunde",
            displayName: "1:mange",
            description: "storkunde"
        },
        {
            id: "savings",
            displayName: "Sparing",
            description: "savings"
        },
        {
            id: "savings_tm",
            displayName: "Sparing for TM",
            description: "savings_tm"
        },
        {
            id: "nyt_90_day_trial",
            displayName: "NYT 90-dagersløp",
            description: "nyt_90_day_trial"
        },
        {
            id: "nyt_bank",
            displayName: "NYT Bank",
            description: "nyt_bank"
        }
    ];
}
