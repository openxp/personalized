var helper = require('/lib/openxp/helper');
var jsonpath = require('/lib/openxp/jsonpath');
var contentLib = require('/lib/xp/content'); // Import the content library
var portalLib = require('/lib/xp/portal'); // Import the portal library
var nbaLib = require('/lib/openxp/nba/index');
var util = require('/lib/enonic/util');

exports.getPersonalizedComponents = function (params) {
    var req = params.req;
    var content = params.content;

    if (!req || !content || !helper.isPreviewOrEditMode(req)) return {};

    var pModel = {
        personalizedComponents: [],
        showGender: false,
        showBirthyear: false,
        showFFA: false,
        showNBA: false,
        showCookie: false,
        showParameter: false,
        showSvCookie: false,
        personalizations: {
            parameters: [],
            cookies: [],
            nbas: []
        },
        iconUrl: portalLib.assetUrl({path: 'personalized/personalized.svg'})
    };


    var mainComponents = [];
    var personalizedComponents = [];
    if (content.page.regions && content.page.regions.main) {
        mainComponents = jsonpath.process(content.page.regions.main, "$..components.*");
        personalizedComponents = mainComponents.filter(function (c) {
            return c.descriptor === app.name + ':personalized'
        });
    }

    /*If a personalized component is stored as a fragment, and re-used across pages, we have to handle this
    * in code since its descriptor is 'fragment' instead of 'personalized'. Fetch the actual content for all fragments
    * on page and copy their config and add them to the personalizedComponents array if they are personalized.*/
    var mainFragmentComponents = mainComponents.filter(function (c) {
        return c.type === 'fragment'
    });
    mainFragmentComponents.forEach(function (fComponent) {
        var fragmentKey = fComponent.fragment;
        var c = contentLib.get({key: fragmentKey});
        try {
            if (c.page.fragment.descriptor === app.name + ':personalized') {
                fComponent.config = c.page.fragment.config;
                personalizedComponents.push(fComponent);
            }
        } catch (e) {

        }
    });

    if (!(content.page.regions && content.page.regions.main)) {
        var singlePersonalizedFragmentComponentEdit = content.page.fragment;
        if (singlePersonalizedFragmentComponentEdit.descriptor === app.name + ':personalized') {
            personalizedComponents.push(singlePersonalizedFragmentComponentEdit);
        }
    }

    //TODO: Rune make sure combination of params AND cookie AND nba are respected
    personalizedComponents.forEach(function (pComponent) {
        var componentModel = {
            path: pComponent.path,
            url: portalLib.componentUrl({component: pComponent.path}),
            personalizedForGender: false,
            personalizedForBirthyear: false
        };
        if (pComponent.path) {
            componentModel.id = helper.removeBadChar(pComponent.path)
        } else {//TODO: Rune: Test solidity of this code. This should only occur when editing a personalized fragment
            componentModel.id = 'singlePersonalizedFragment';
        }

        var personalizations = nbaLib.getPersonalizations({req: req, component: pComponent});
        if (personalizations) {
            if (personalizations.parameters) {
                personalizations.parameters.forEach(function (param) {
                    pModel.personalizations.parameters.push(param)
                });
            }
            if (personalizations.cookies) {
                personalizations.cookies.forEach(function (cookie) {
                    pModel.personalizations.cookies.push(cookie)
                });
            }
            if (personalizations.nbas) {
                personalizations.nbas.forEach(function (nba) {
                    if (nba.gender) {
                        pModel.showGender = true;
                        componentModel.personalizedForGender = true;
                    }
                    if (nba.bornEarliest || nba.bornLatest) {
                        pModel.showBirthyear = true;
                        componentModel.personalizedForBirthyear = true;
                    }
                    if (nba.key) {
                        pModel.showNBA = true;
                        componentModel.personalizedForNba = true;
                        var nbaKeys = util.data.trimArray(nba.key);
                        nbaKeys.forEach(function (nbaKey) {
                            if (pModel.personalizations.nbas.indexOf(nbaKey) === -1) {
                                pModel.personalizations.nbas.push(nbaKey);
                            }
                        });

                    }
                });
            }
        }

        pModel.personalizedComponents.push(componentModel);
    });
    return pModel;
};