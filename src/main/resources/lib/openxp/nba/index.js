var util = require('/lib/enonic/util');
var portalLib = require('/lib/xp/portal');
var jsonPath = require('/lib/openxp/jsonpath');


/**
 * @typedef {Object} personalizations
 * @property {string} key The link url
 * @property {string} target The link _target attribute
 * @property {string} text The text of the link (default:displayName)
 */
exports.getPersonalizations = function(params){
    if (!params.req)return;
    var req = params.req;
    var componentConfig = {};
    if (params.component && params.component.config){
        componentConfig = params.component.config;
    } else{
        componentConfig = portalLib.getComponent().config;
    }

    if (!componentConfig.personalizations){
        return false;
    }
    var personalizationsConfig = util.data.trimArray(componentConfig.personalizations);

    var reqParams = req.params;
    var reqCookies = req.cookies;
    var stbIdValue = jsonPath.process(reqCookies, '$..stb_id')[0];
    var reqSvProductCookies;

    var personalizations = {
        stbId: (componentConfig.stbId && stbIdValue),
        nbas:[],
        parameters: [],
        cookies: [],
        //Will return highest prioritized content in personalization config based on cookies or parameters in request
        checkForNBAMatch:false,
        svProductMatch:false,
        campaignMatch:false,
        personalizedContentRef:false,
        matches:[]
    };

    //Check if personalizations is configured to use smartere valg personalization (optional)
    if (componentConfig.svProduct){
        //Check if sv_product cookie is set in session
        var svProductValue = jsonPath.process(reqCookies, '$..sv-product')[0];
        //If sv_product is comma separated, turn it into an array
        if (svProductValue && svProductValue.indexOf(',') !== -1){
            svProductValue = svProductValue.split(',');
        }
        if (svProductValue){
            reqSvProductCookies = util.data.forceArray(svProductValue);
        }
    }

    /*Add configured nba's and check sv_product, cookies and parameters against request object*/
    for (var i = 0; i < personalizationsConfig.length; i++) {
        var selectedOptions = util.data.forceArray(personalizationsConfig[i]._selected);

        for (var so = 0; so < selectedOptions.length; so++) {
            var selectedOption = selectedOptions[so];
            if (!selectedOption)continue;
            if (!personalizationsConfig[i][selectedOption])continue;
            if (!personalizationsConfig[i][selectedOption].content)continue;

            var content = personalizationsConfig[i][selectedOption].content;
            var nbaEntries = util.data.trimArray(personalizationsConfig[i][selectedOption]);
            var keys = util.data.trimArray(personalizationsConfig[i][selectedOption].key);
            var value = personalizationsConfig[i][selectedOption].value;

            if (personalizationsConfig[i] === undefined
                || selectedOption === undefined
                || content === undefined
                || (nbaEntries.length < 1 && keys < 1)) {
                continue;
            }
            /*Add configured nba's in prioritized order*/
            if (selectedOption === 'nbacode'){
                for (var n = 0; n < nbaEntries.length; n++) {
                    if (nbaEntries[n].key){
                        //Check for sv_product cookie match
                        if (reqSvProductCookies){
                            var svProductMatch = reqSvProductCookies.indexOf(keys[n]) > -1;
                            if (svProductMatch && !personalizations.svProductMatch){
                                var svProductMatch = {
                                    type: 'sv',
                                    key: keys[n]
                                };
                                if (keys[n]){
                                    svProductMatch.attrAppend += ',data-personalized-source=sv';
                                    svProductMatch.attrAppend += ',data-personalized-key='+keys[n];
                                }
                                personalizations.matches.push(svProductMatch);
                                personalizations.svProductMatch = true;

                                if (!personalizations.personalizedContentRef){
                                    personalizations.personalizedContentRef = content;
                                }
                            }
                        }
                    }
                    var nbaChecks = {
                        contentRef: content //content to display if there is a match
                    };
                    //log.debug("nbaEntry %s",nbaEntries[n]);
                    if (nbaEntries[n].key){
                        nbaChecks.key = nbaEntries[n].key;
                    }
                    if (nbaEntries[n].personalizedFilter){
                        var selected = util.data.forceArray(nbaEntries[n].personalizedFilter._selected);
                        selected.forEach(function (s){
                            if (s === 'gender'){
                                nbaChecks.gender = nbaEntries[n].personalizedFilter[s].gender;
                            }else if (s === 'age'){
                                if (nbaEntries[n].personalizedFilter[s].bornEarliest){
                                    nbaChecks.bornEarliest = nbaEntries[n].personalizedFilter[s].bornEarliest;
                                }if (nbaEntries[n].personalizedFilter[s].bornLatest){
                                    nbaChecks.bornLatest = nbaEntries[n].personalizedFilter[s].bornLatest;
                                }
                            }else if (s === 'ffa'){
                                nbaChecks.ffa = nbaEntries[n].personalizedFilter[s].ffa;
                            }
                        });
                    }
                    personalizations.nbas.push(nbaChecks);
                }
            }
            //TODO: Rune Fix this as all parameters / cookies are listed in personalized object now
            else if (selectedOption === 'parameter' || selectedOption === 'cookie'){
                var reqKeys;
                if (selectedOption === 'parameter') {
                    reqKeys = reqParams;
                }
                else if (selectedOption === 'cookie') {
                    reqKeys = reqCookies;
                }
                for (var reqKey in reqKeys) {
                    var reqKeyMatch = keys.indexOf(reqKey) !== -1;
                    if (!reqKeyMatch)continue;

                    //value is optional
                    var sessionParam = jsonPath.process(params, '$..'+reqKey)[0];
                    if (sessionParam!==undefined){
                        //Check for correct param / cookie value if set, also possible to use param without specific value
                        if (value && sessionParam !== value){
                            continue;
                        }
                        personalizations[selectedOption+'s'].push({key:reqKey, value:value, contentRef:content});
                        if (!personalizations.campaignMatch){
                            personalizations.campaignMatch = true;
                            if (!personalizations.personalizedContentRef){
                                personalizations.personalizedContentRef = content;
                            }
                            var campaignMatch = {
                                contentRef: content,
                                type: selectedOption,
                                key: reqKey,
                                value: value
                            };
                            if (selectedOption){
                                campaignMatch.attrAppend = ',data-personalized-source='+selectedOption;
                            }
                            if (reqKey){
                                campaignMatch.attrAppend += ',data-personalized-key='+reqKey;
                                if (value){
                                    campaignMatch.attrAppend += ',data-personalized-value='+value;
                                }
                            }

                            personalizations.matches.push(campaignMatch);
                        }

                    }
                }
            }
        }
    }
    if (personalizations.stbId && personalizations.nbas.length > 0){
        personalizations.checkForNBAMatch = true;
    }
    return personalizations;
};