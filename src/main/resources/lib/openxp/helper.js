var portalLib = require('/lib/xp/portal');
var util = require('/lib/enonic/util');

exports.getAppUrl = function getAppUrl() {
    return portalLib.url({path:'/app/' + app.name});
};

exports.getBaseUrl = function() {
    var appUrl = this.getAppUrl();
    var baseUrl = this.endsWithSlash(appUrl) ? appUrl.slice(0, -1) : appUrl;
    
    return baseUrl;
};

exports.endsWithSlash = function(url) {
    return url.slice(-1) === '/';
};

/*
    @param req
*/
exports.isPreviewOrEditMode = function (req) {
    if (!req || !req.mode) return false;
    return req && (req.mode === 'preview' || req.mode === 'edit');
};

exports.isEmptyObject = function(obj){
    return (Object.keys(obj).length === 0 && obj.constructor === Object)
};

exports.isEmptyArray = function (arr) {
    return util.data.trimArray(arr).length < 1;
};

exports.removeBadChar = function (str) {
    if (str===undefined){
        return '';
    }
    return str.replace(/[^a-z0-9_]/ig, '-').toLowerCase();
};
